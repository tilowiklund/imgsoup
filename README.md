# Install in virutalenv

In bash
```
git clone https://gitlab.com/tilowiklund/imgsoup.git
cd imgsoup
virtualenv .
source bin/activate
pip install -r requirements.txt
```
# Usage

```
usage: imgsoup [-h] [--root ROOT] FILEs [FILEs ...]

Find broken image links.

positional arguments:
  FILEs        Files to look for broken links in

optional arguments:
  -h, --help   show this help message and exit
  --root ROOT  root www directory (to figure out absolute links)
```

# Usage example

```
./imgsoup --root ./html_root ./html_root/page1.html ./html_root/page2.html ...
```

Using find to apply to multiple files
```
find ./html_root -iname '*.html' -exec ./imgsoup --root ./html_root {} \+
```

Should produce output along the lines of
```
100%|█████████████████████████████████████████████████████████████████████████████████████████████| 3/3 [00:00<00:00, 3815.32it/s]

====== Results: =======
External images: 1
Unknown images: 0
Local images (existing): 0
Local images (missing): 2

=== Missing files: ===
/path/to/imgsoup/idonotexist.png
 appears in './html_root/page2.html' as '../idonotexist.png'
/path/to/imgsoup/html_root/absolute/path.png
 appears in './html_root/page3.html' as '/absolute/path.png'
```
